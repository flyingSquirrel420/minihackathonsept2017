import logging
from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session

app = Flask(__name__)

ask = Ask(app, "/")

logging.getLogger("flask_ask").setLevel(logging.DEBUG)


@ask.launch
def new_tree():
    msg = render_template('welcome')
    return question(msg)

@ask.intent("YesIntent")
def sayYes():
    msg = render_template('YesIntent')
    return question(msg)

@ask.intent("NoIntent")
def sayNo():
    msg = render_template('NoIntent')
    return statement(msg)

@ask.intent("IvankaTrump")
def ivanka():
    msg = render_template('IvankaTrump')
    return question(msg)

@ask.intent("EricTrump")
def eric():
    msg = render_template('EricTrump')
    return question(msg)

@ask.intent("MarlaMaples")
def marla():
    return question("Marla Maples is an American actress and television personality. She was also the second wife to Donald from 1993 to 1999. Would you like to know anyone else in his family?")

@ask.intent("IvanaTrump")
def ivana():
    msg = render_template("IvanaTrump")
    return question(msg)

@ask.intent("BarronTrump")
def barron():
    msg = render_template("BarronTrump")
    return question(msg)

@ask.intent("DonaldJunior")
def junior():
    msg = render_template("DonaldJunior")
    return question(msg)

@ask.intent("TiffanyTrump")
def tiffany():
    msg = render_template("TiffanyTrump")
    return question(msg)

if __name__ == '__main__':

    app.run(debug=True)
